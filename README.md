# Documentazione
## Struttura delle cartelle
* `build` - contiene i file di configurazione dei tool utilizzati.
    * `.stylelintrc`
    * `postcss.config.js`
* `css` - i file bundle generati
    * `main.css`
    * `main.css.map`
    * `main.css.min`
    * `main.css.min.map`
* `scss`
    * `_bootstrap_components.scss` - componenti di bootstrap inclusi nel bundle finale.
    * `_custom.scss` - regole css personalizzate ed eventuali inclusioni di altri file che aggiungono altri componenti o semplicemente altre regole.
    * `_variables.scss` - variabili clonate da bootstrap per la sovrascrittura e variabili custom.
    * `main.scss` - inclusione di tutti i file scss.

---
## Package.json

Ogni script che segue va eseguito nella console e deve essere preceduto dal comando `npm run`.

|Script|descrizione|tool|config|
|:---:|:---|:---:|---:|
|`css`|esegue tutti i task css seguenti con un unico comando e nel seguqnte ordine: ~~`css.lint`~~, `css.compile`, `css.prefix`, `css.minify`.|-|-|
|`css.with.lint`|Come sopra ma aggiunge alla catena di esecuzione anche il controllo del codice.|-|-|
|`css.lint`|controlla eventuali errori del scss e verifica se le best practices sono state rispettate (coming soon).|`stylelint`|`build/.stylelintrc`|
|`css.compile`|compila tutti i file `scss` in un unico file css dentro la cartella `/css` chiamato `main.css`. Crea anche il rispettivo file di mappa `main.css.map`.|`node-sass`|`inline`|
|`css.prefix`|esamina il file `css/main.css` aggiungendo, dove necessario, i prefissi per rendere i file di stile crossbrowser.|`postcss`|`package.json`, `build/postcss.config.js`|
|`css.minify`|prende in ingresso `css/main.css` e lo comprime, eliminando commenti e righe vuote e portando tutto il codice css in un unica riga.|`cleancss`|`inline`|
|`css.watch`|osserva tutti i file contenuti in `scss/**/*.scss` controllandone la sintassi con `lint`, appena intercetta un cambiamento di uno di questi file riavvia lo script `npm run css`.|`watch`|-|
|`css.watch.no.lint`|osserva tutti i file contenuti in `scss/**/*.scss`, appena intercetta un cambiamento di uno di questi file riavvia lo script `npm run css`.|`watch.no.lint`|-|

---
## Link utili

* Link Documentazioni
    * [Bootstrap](http://getbootstrap.com/docs/4.1/getting-started/introduction/)
    * [Nodejs](https://nodejs.org/it/docs/)
    * [stylelint](https://stylelint.io/user-guide/)
    * [node-sass](https://www.npmjs.com/package/node-sass)
    * [postcss](https://github.com/postcss/postcss/tree/master/docs)
        * [post-css-cli](https://github.com/postcss/postcss-cli)
    * [clean-css](https://github.com/jakubpawlowicz/clean-css)
        * [clean-css-cli](https://github.com/jakubpawlowicz/clean-css-cli)
    * [watch](https://github.com/mikeal/watch)

---
## Funzionamento Lint
Lint viene eseguito ad ogni compilazione e verifica la sintassi del codice all'interno dei file `.scss`.
La violazione delle regole sulla sintassi può generare `error` o `warning`: Gli errori sono bloccanti e impediscono la compilazione.

Regole Applicate:

|Regola|Descrizione|Severità|
|:---|:---|:---:|
|`indentation : tab`|Richiede che l'indentazione sia un tab |`warning`|
|`at-rule-name-space-after : always`|Richiede un singolo spazio dopo il nome di una regola |`error`|
|`at-rule-no-vendor-prefix : true`|Non consente l'uso di prefissi specifici per i diversi browser nelle regole|`error`|
|`at-rule-semicolon-space-before : never`|Non consente spazi prima del carattere ";" |`error`|
|`color-named : never`|Non consente di utilizzare i nomi dei colori |`error`|
|`declaration-block-semicolon-newline-after : always-multi-line`|Richiede una nuova linea dopo il carattere ";" nelle regole multilinea |`error`|
|`declaration-block-semicolon-newline-before : never-multi-line`|Non consente spazi bianchi prima del carattere ";" nelle regole multilinea |`error`|
|`declaration-block-semicolon-space-after : always-single-line`|Richiede uno singolo spazio dopo il carattere ";" nelle regole su un'unica linea |`error`|
|`declaration-no-important : true`|Non consente l'utilizzo della parola chiave `!important` |`warning`|
|`font-family-name-quotes : always-where-recommended`|Richiede le virgolette per i font-family |`error`|
|`font-weight-notation : numeric`|Richiede che il valore della regola `font-weight` sia espressa numericamente |`error`|
|`function-url-no-scheme-relative : true`|Non consente l'utilizzo di URL relativi. Es: "//index.html" non è consentito |`error`|
|`function-url-quotes": always`|Richiede l'utilizzo degli apici per gli URL |`error`|
|`length-zero-no-unit : true`|Non consente l'uso dell'unità di misura se il valore dell'attributo è "0" (Es: 0px) |`error`|
|`max-empty-lines : 2`|Non consente più di 2 linee vuote |`error`|
|`media-feature-name-no-vendor-prefix : true`|Non consente la dichiarazione di regole specifiche per i diversi browser nelle regole `@media` |`error`|
|`media-feature-parentheses-space-inside : never`|Non consente spazi bianchi tra le parentesi e le regole nelle media query |`error`|
|`media-feature-range-operator-space-after : always`|Richiede uno spazio bianco dopo l'operatore nelle media query |`error`|
|`media-feature-range-operator-space-before : never`|Non consente spazi bianchi prima dell'operatore nelle media query |`error`|
|`no-duplicate-selectors : true`|Non consente selettori duplicati |`error`|
|`number-leading-zero : never`|Non consente l'uso dello "0" nei casi in cui il valore è minore di 0 (Es: 0.5) |`error`|
|`media-feature-name-no-unknown : true`|Non consente l'uso della parola chiave `unknown` nelle media query |`error`|
|`order/properties-order : []`|Stabilisce l'ordine di dichiarazione delle regole CSS |`error`|
|`property-no-vendor-prefix : true`|Non consente l'uso di prefissi specifici per i diversi browser nelle proprietà |`error`|
|`selector-attribute-quotes : always`|Richiede che i valori degli attributi siano tra virgolette |`error`|
|`selector-list-comma-newline-after : always`|Richiede una nuova linea dopo il carattere "," |`error`|
|`selector-list-comma-newline-before : never-multi-line`|Non consente spazi bianchi prima del carattere "," nelle liste di selettori a più linee |`error`|
|`selector-list-comma-space-after : always-single-line`|Richiede un singolo spazio dopo il carattere "," nelle liste di selettori su una sola linea |`error`|
|`selector-list-comma-space-before : never-single-line`|Non consente spazi bianchi prima del carattere "," nelle liste di selettori su una sola linea |`error`|
|`selector-max-attribute : 2`|Limita il numero di attributi in un selettore a 2 |`error`|
|`selector-max-class : 4`|Limita il numero di classi in un selettore a 4 |`error`|
|`selector-max-combinators : 4`|Limita il numero di combinatori in un selettore a 4 |`error`|
|`selector-max-compound-selectors : 4`|Limita il numero di combinatori composti a 4 |`error`|
|`selector-max-empty-lines : 0`|Non consente linee vuote tra i selettori |`error`|
|`selector-max-type : 2`|Limita il numero di selettori a 2 |`error`|
|`selector-max-universal : 1`|Limita il numero di selettori universali (*) a 1 |`error`|
|`selector-no-qualifying-type : true`|Non consente la qualificazione di un selettore per tipo (Es: a.foo {..}) |`error`|
|`selector-no-vendor-prefix : true`|Non consente l'uso di prefissi specifici per i diversi browser nei selettori |`error`|
|`string-quotes : double`|Richiede i doppi apici per le stringhe |`error`|
|`value-keyword-case : lower`|Non consente caratteri maiuscoli nelle keyword per le regole |`error`|
|`value-list-comma-newline-after : never-multi-line`|Non consente spazi dopo il carattere "," nelle liste di valori su più linee |`error`|
|`value-list-comma-newline-before : never-multi-line`|Non consente spazi prima del carattere "," nelle liste di valori su più linee |`error`|
|`value-list-comma-space-after : always`|Richiede sempre uno spazio dopo il carattere "," |`error`|
|`value-no-vendor-prefix : true`|Non consente l'uso di prefissi specifici per i diversi browser come valori |`error`|

List [order/properties-order](./CSSRulesOrder.md).  
Tutte le regole di sintassi scss sono contenute in `build/.stylelintrc`.

## Configurazione Vscode:
E' possibile configurare alcune caratteristiche di base per utilizzare il tool di formattazione automatico del documento in accordo alle regole Lint sopra elencate. Per fare questo occorre creare il file `.jsbeautifyrc` nella root del progetto contenente:

```javascript
{
	"indent_size": 1,
	"indent_char": "\t",
	"css": {
		"indent_size": 1,
		"end_with_newline": true
	}
}
```

## ToDo list
* Revisionare il file `build/stylelintrc` sulla base delle regole che si vogliono applicare alla scrittura del nostro codice scss.
